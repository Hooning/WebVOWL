var express = require('express');
var router = express.Router();
var request = require('request');
var FormData = require('form-data');
const formidable = require('formidable');
var fs = require('fs');
var path = require('path');
const { endpoint, port } = require('../config');

var ipAddress = 'localhost';

console.log('Host : ', endpoint);
console.log('Port : ', port);


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/viewGraph', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="viewGraph" method="post" enctype="multipart/form-data">');
    res.write('<input type="file" name="file"><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end();
})

router.post('/viewGraph', async (req, res, next) => {
    
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        var oldpath = files.files.path;
        var fileName = files.files.name;
        var fileType = files.files.type;
        var tempPath = path.join(__dirname, "../data/", fileName);

        fs.copyFile(oldpath, tempPath, function (err) {
            if (err) console.log(err)
            console.log('# webvowl_data_move: File stored to temp directory');
        });

        if (!!endpoint) {
            ipAddress = endpoint;
        }

        // send the file to webvowl directory
        const formData = {
            webvowlGraphFile: {
              value:  fs.createReadStream(tempPath),
              options: {
                filename: fileName,
                contentType: fileType
              }
            }
        };
        
        request.post({url:'http://' + ipAddress + ':8020/fileUpload', formData: formData}, function optionalCallback(err, httpResponse, body) {
            if (err) {
                return console.error('# webvowl_data_move: Upload failed:', err);
            }

            if (body) {
                console.log('# webvowl_data_move: Upload successful! body');
                console.log('ipAddress: ', ipAddress);
                setTimeout(() => {
                    res.writeHead(200,
                        { Location: 'http://' + ipAddress + ':8000/#' + fileName.substr(0, fileName.lastIndexOf(".")) }
                    );
                    res.write('# webvowl_data_move: File uploaded and moved!');
                    res.end();
                }, 500);
            }
            
        });
    }); 
}); 

module.exports = router;
