const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  endpoint: process.env.HOST,
  port: process.env.PORT
};