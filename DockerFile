FROM node:12
WORKDIR /app
COPY package.json /app
ENV HOST 172.18.0.1
ENV PORT 8000
RUN npm install \
    && npm install grunt-cli -g
COPY . /app
CMD grunt webserver
EXPOSE 8000 8020