var express = require('express');
var multiparty = require('multiparty');
var fs = require('fs');
var path = require('path');
var app = module.exports = express();

app.listen(8020, () => {
    console.log("# webvowl: Server running on port ", 8020);
})

app.post('/fileUpload', (req, res) => {
    var form = new multiparty.Form();
    form.parse(req, function (err, fields, files) {
      var originPath = files.webvowlGraphFile[0].path;
      var fileName = files.webvowlGraphFile[0].originalFilename;
      var newPath = path.join(__dirname, "/../data/", fileName);
      
      fs.copyFile(originPath, newPath, function (err) {
          if (err) console.log(err)
          console.log('# webvowl: File stored to directory');
          res.send(true);
      });
    });
  })
